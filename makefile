GOROOT := $(shell go env GOROOT)
GO111MODULE=auto
ARCH=$(uname -p)
OS := $(shell uname)
ARCH := $(shell uname -m)

all: wasm-rotating-cube

deps:
	mkdir build || true
	GOOS=js GOARCH=wasm go get "github.com/bobcob7/wasm-rotating-cube/gltypes" github.com/go-gl/mathgl/mgl32 github.com/donomii/sceneCamera 

wasm-rotating-cube: deps output.wasm server/main.go
	echo $(OS)
	go build -o "build/wasm-server-$(OS)-$(ARCH)" server/main.go

output.wasm: bundle.go wasm_exec.js
	GOOS=js GOARCH=wasm go build -o build/bundle.wasm bundle.go

run: output.wasm wasm-rotating-cube
	./wasm-rotating-cube

wasm_exec.js:
	cp $(GOROOT)/misc/wasm/wasm_exec.js .

clean:
	rm -f wasm-rotating-cube wasm_exec.js *.wasm
